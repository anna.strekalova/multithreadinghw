﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace MultithreadingHW
{
    class Program
    {
        static void Main(string[] args)
        {
            var size100T = 100000;
            var size1M = 1000000;
            var size10M = 10000000;

            var data = RandomGenerator(size100T);

            Stopwatch sw = new Stopwatch();

            Console.WriteLine($"Начало работы последовательного вычисления суммы целочисленных элементов...");
            sw.Start();
            var sum1 = new MathOperation().SumListElements(data);
            sw.Stop();
            Console.WriteLine($"Результат сложения: {sum1}. Время выполнения: {sw.Elapsed.TotalMilliseconds} мс");
            sw.Reset();

            Console.WriteLine($"Начало работы параллельного вычисления суммы целочисленных элементов...");
            sw.Start();
            var sum2 = new ThreadMathOperation(4).SumListElements(data);
            sw.Stop();
            Console.WriteLine($"Результат сложения: {sum2}. Время выполнения: {sw.Elapsed.TotalMilliseconds} мс");
            sw.Reset();

            Console.WriteLine($"Начало работы вычисления суммы целочисленных элементов с использованием PLINQ...");
            var sum3 = new PLINQMathOperation().SumListElements(data);

            Console.ReadKey();
        }

        private static List<int> RandomGenerator(int size)
        {
            List<int> numbers = new List<int>(size);
            var rand = new Random();

            for(var i =0; i < size; i++)
            {
                numbers.Add(rand.Next(10));
            }
            
            return numbers;
        }
    }
}
