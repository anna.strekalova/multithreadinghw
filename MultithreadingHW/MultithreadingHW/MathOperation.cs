﻿using MultithreadingHW.Interface;
using System.Collections.Generic;

namespace MultithreadingHW
{
    public class MathOperation : IMathOperation
    {
        public long SumListElements(List<int> numbers)
        {
            var sum = 0;

            foreach (var number in numbers)
            {
                sum += number;
            }

            return sum;
        }
    }
}
